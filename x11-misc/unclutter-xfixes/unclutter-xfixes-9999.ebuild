# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit git-r3

DESCRIPTION="Fix of unclutter using the xfixes extension"
HOMEPAGE="https://github.com/Airblader/unclutter-xfixes"
LICENSE="MIT"
SLOT=0
EGIT_REPO_URI="git://github.com/Airblader/unclutter-xfixes.git"

RDEPEND="dev-libs/libev"
